package com.kmarinos.amzmockproducts.api;

import com.kmarinos.amzmockproducts.api.dao.AddressDTO;
import com.kmarinos.amzmockproducts.api.dao.AddressGET;
import com.kmarinos.amzmockproducts.api.dao.AddressPOST;
import com.kmarinos.amzmockproducts.api.dao.CustomerDTO;
import com.kmarinos.amzmockproducts.api.dao.CustomerGET;
import com.kmarinos.amzmockproducts.api.dao.CustomerPOST;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.service.AddressService;
import com.kmarinos.amzmockproducts.service.CustomerService;
import com.kmarinos.amzmockproducts.service.process.ProcessingQueue;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CustomerResource {

  private final CustomerService customerService;
  private final AddressService addressService;
  private final ProcessingQueue processingQueue;

  public CustomerResource(
      CustomerService customerService,
      AddressService addressService,
      @Qualifier("customerQueue") ProcessingQueue processingQueue) {

    this.customerService = customerService;
    this.addressService = addressService;
    this.processingQueue = processingQueue;
  }

  @GetMapping("customers")
  public List<CustomerGET> getAllCustomers() {
    return CustomerDTO.GET(customerService.getAllCustomers());
  }

  @GetMapping("customers/{id}")
  public CustomerGET getCustomerById(@PathVariable("id") String id) {
    return CustomerDTO.GET(
        customerService.getCustomerById(id, addressService::getAddressesByCustomer));
  }

  @GetMapping("customers/{id}/addresses")
  public List<AddressGET> getAddressesForCustomer(@PathVariable("id") String id) {
    return AddressDTO.GET(
        addressService.getAddressesByCustomer(customerService.getCustomerById(id)));
  }

  @GetMapping("addresses/{id}")
  public AddressGET getAddressById(@PathVariable("id") String id) {
    return AddressDTO.GET(addressService.getAddressById(id));
  }

  @PostMapping("customers/{id}/addresses")
  public AddressGET createAddressForCustomer(
      @PathVariable("id") String customerId, @RequestBody AddressPOST addressPOST) {
    Customer customer = customerService.getCustomerById(customerId);
    return AddressDTO.GET(addressService.createAddressForCustomer(customer, addressPOST));
  }

  @PostMapping("customers")
  //  public CustomerGET createCustomer(@RequestBody CustomerPOST customerPOST){
  //
  //    return this.getCustomerById(customerService.createCustomer(customerPOST,cus->{
  //      addressService.createAddressForCustomer(cus,customerPOST.getAddress());
  //    }).getId());
  //  }
  public ResponseEntity<Void> createCustomerPlaceholder(@RequestBody CustomerPOST customerPOST) {
    //    customerService.createCustomerPlaceholder();
    processingQueue.process(
        () -> {
          customerService.createCustomer(
              customerPOST,
              cus -> {
                addressService.createAddressForCustomer(cus, customerPOST.getAddress());
              });
        });

    return ResponseEntity.status(201).build();
  }

  @GetMapping("/login")
  public CustomerGET getRandomCustomer() {
    return CustomerDTO.GET(customerService.getRandomCustomers(1).get(0));
  }
}
