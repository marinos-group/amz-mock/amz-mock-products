package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SalesOrderDTO {
  /********** POST ***********/
  /*public static SalesOrder POST(SalesOrderPOST salesOrderPOST){

    return SalesOrder.builder()

        .build();
  }/*

  /********** GET ***********/

  public static SalesOrderGET GET(SalesOrder salesOrder) {
    return GET(
        salesOrder,
        buildAttributes(salesOrder),
        CustomerDTO::referenced,
        AddressDTO::referenced,
        AddressDTO::referenced,
        SalesOrderItemDTO::GET);
  }

  public static List<SalesOrderGET> GET(List<SalesOrder> salesOrders) {
    return salesOrders.stream().map(SalesOrderDTO::inCollection).collect(Collectors.toList());
  }

  private static SalesOrderGET GET(
      SalesOrder salesOrder,
      SalesOrderGET.SalesOrderGETBuilder salesOrderGETBuilder,
      Function<Customer, CustomerGET> handleCustomer,
      Function<Address, AddressGET> handleDeliverTo,
      Function<Address, AddressGET> handleInvoiceTo,
      Function<List<SalesOrderItem>, List<SalesOrderItemGET>> handleSalesOrderItems) {

    if (salesOrder == null) {
      return null;
    }
    return salesOrderGETBuilder
        .customer(handleCustomer.apply(salesOrder.getCustomer()))
        .deliverTo(handleDeliverTo.apply(salesOrder.getDeliverTo()))
        .invoiceTo(handleInvoiceTo.apply(salesOrder.getInvoiceTo()))
        .orderItems(handleSalesOrderItems.apply(salesOrder.getSalesOrderItems()))
        .build();
  }

  protected static SalesOrderGET.SalesOrderGETBuilder buildSimple(SalesOrder salesOrder) {
    if (salesOrder == null) {
      return SalesOrderGET.builder();
    }
    return SalesOrderGET.builder().id(salesOrder.getId()).status(salesOrder.getStatus());
  }

  protected static SalesOrderGET.SalesOrderGETBuilder buildAttributes(SalesOrder salesOrder) {
    return buildSimple(salesOrder).invoicedAmount(salesOrder.getInvoicedAmount());
  }

  protected static SalesOrderGET inCollection(SalesOrder salesOrder) {
    SalesOrderGET.SalesOrderGETBuilder builder = buildAttributes(salesOrder);
    return GET(
        salesOrder,
        builder,
        CustomerDTO::referenced,
        AddressDTO::referenced,
        AddressDTO::referenced,
        list -> null);
  }

  protected static SalesOrderGET referenced(SalesOrder salesOrder) {
    return GET(
        salesOrder,
        buildSimple(salesOrder),
        cus -> null,
        deliverTo -> null,
        invoiceTo -> null,
        items -> null);
  }

  protected static List<SalesOrderGET> referenced(Collection<SalesOrder> salesOrders) {
    if (salesOrders == null) {
      return null;
    }
    return salesOrders.stream().map(SalesOrderDTO::referenced).collect(Collectors.toList());
  }
}
