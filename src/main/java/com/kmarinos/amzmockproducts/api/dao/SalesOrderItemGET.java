package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.amzmockproducts.model.SalesOrderItemStatus;
import com.kmarinos.amzmockproducts.model.SalesOrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class SalesOrderItemGET {

  String id;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  SalesOrderGET salesOrder;
  ProductGET product;
  SalesOrderItemStatus status;
  long quantity;
  double pricePerItem;
  double price;
}
