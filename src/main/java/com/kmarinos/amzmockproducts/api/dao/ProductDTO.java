package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Department;
import com.kmarinos.amzmockproducts.model.Product;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductDTO {

  public static ProductGET GET(Product product) {
    return GET(product, buildAttributes(product), DepartmentDTO.departmentWithCategory);
  }

  public static List<ProductGET> GET(List<Product> products) {
    return products.stream()
        .map(ProductDTO::inCollection)
        .collect(Collectors.toList());
  }

  private static ProductGET GET(
      Product product,
      ProductGET.ProductGETBuilder productGETBuilder,
      Function<Department, DepartmentGET> handleDepartment) {

    if (product == null) {
      return null;
    }
    return productGETBuilder.department(handleDepartment.apply(product.getDepartment())).build();
  }

  public static ProductGET.ProductGETBuilder buildSimple(Product product) {
    if (product == null) {
      return ProductGET.builder();
    }
    return ProductGET.builder().id(product.getId()).descriptionShort(product.getDescriptionShort());
  }

  public static ProductGET.ProductGETBuilder buildAttributes(Product product) {
    return buildSimple(product)
        .descriptionLong(product.getDescriptionLong())
        .url(product.getUrl())
        .rating(product.getRating().getTotalRatings()==0?null:product.getRating())
        .price(product.getPrice());
  }

  public static ProductGET inCollection(Product product){
    ProductGET.ProductGETBuilder builder = buildSimple(product)
        .price(product.getPrice()).url(product.getUrl());
    return GET(product, builder, DepartmentDTO.departmentWithCategory);

  }
  public static ProductGET referenced(Product product) {
    return GET(product, buildSimple(product), dep -> null);
  }
}
