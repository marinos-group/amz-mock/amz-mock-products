package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DTOFactory<T> {

  Map<Class<?>, Function> mappers = new HashMap<>();
  List<MappedFunction> listedMappers = new ArrayList<>();
  private static final List<Map<Object, Object>> alreadyMapped = new ArrayList<>();

  public <U> void registerMapper(Class<U> clazz, Function<U, ?> mapper) {
//    mappers.put(clazz, mapper);
    registerMapper(clazz,null,mapper);
  }

  public <U> void registerMapper(Class<U> clazz, String forFieldName, Function<U, ?> mapper) {
//    mappers.put(clazz, mapper);
    listedMappers.add(MappedFunction.builder()
        .forClass(clazz)
        .forFieldName(forFieldName)
        .mapper(mapper)
        .build());
  }

  public Function find(Class<?> forClass, String forFieldName) {
    if (forClass == null) {
      return null;
    }
    if (forFieldName == null) {
      return listedMappers.stream()
          .filter(mf -> mf.getForClass().equals(forClass) && mf.getForFieldName() == null)
          .map(MappedFunction::getMapper)
          .findFirst()
          .orElse(null);
    } else {
      List<MappedFunction> candidates =
          listedMappers.stream()
              .filter(mf -> mf.getForClass().equals(forClass))
              .collect(Collectors.toList());
      Function generalMapperOfType =
          candidates.stream()
              .filter(mf -> mf.getForFieldName() == null)
              .map(MappedFunction::getMapper)
              .findFirst()
              .orElse(null);
      return candidates.stream()
          .filter(mf -> forFieldName.equals(mf.getForFieldName()))
          .findFirst()
          .map(MappedFunction::getMapper)
          .orElse(generalMapperOfType);
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  private static class MappedFunction {
    Class<?> forClass;
    String forFieldName;
    Function mapper;
  }

  public <R> R GET(T seed, Class<R> dtoClass) {
    return GET(seed, dtoClass, null);
  }

  public <R> R GET(T seed, Class<R> dtoClass, List<String> fields) {

    try {
      BiPredicate<PropertyDescriptor, List<String>> filter =
          (pd, list) -> {
            if (list == null) {
              return true;
            } else {
              return list.contains(pd.getName());
            }
          };
      Map<String, PropertyDescriptor> dtoFields =
          Arrays.stream(Introspector.getBeanInfo(dtoClass).getPropertyDescriptors())
              .filter(pd -> filter.test(pd, fields))
              .collect(Collectors.toMap(PropertyDescriptor::getName, Function.identity()));
      final var dto = dtoClass.getConstructor().newInstance();
      if (seed != null) {
        alreadyMapped.add(Map.of(seed, dto));
      }
      for (PropertyDescriptor pd :
          Introspector.getBeanInfo(seed.getClass()).getPropertyDescriptors()) {
        if (pd.getReadMethod() != null && !"class".equals(pd.getName())) {
          Optional.ofNullable(dtoFields.get(pd.getName()))
              .ifPresent(
                  field -> {
                    try {
                      // check if field types match
                      log.info(
                          "Comparing origin {} with target {}",
                          pd.getPropertyType(),
                          field.getPropertyType());
                      if (pd.getPropertyType().equals(field.getPropertyType())) {
                        // check if parameter types also match
                        final var genericType =
                            seed.getClass().getDeclaredField(pd.getName()).getGenericType();
                        // classes are the same and they are defined without generics
                        if (!(genericType instanceof ParameterizedType)) {
                          field.getWriteMethod().invoke(dto, pd.getReadMethod().invoke(seed));
                        }
                        // It contains a Generic type
                        else {
                          var parameterizedType = (ParameterizedType) genericType;
                          final var genericTypeTarget =
                              (ParameterizedType)
                                  dto.getClass().getDeclaredField(pd.getName()).getGenericType();
                          // if generics are also of the same class just copy the object;
                          if (parameterizedType.equals(genericTypeTarget)) {
                            field.getWriteMethod().invoke(dto, pd.getReadMethod().invoke(seed));
                          } else {
                            // check if collection mapper exists
                            applyMappers(seed, dto, pd, field);
                          }
                        }
                      }
                      // check if mapper exists
                      else {
                        applyMappers(seed, dto, pd, field);
                      }

                    } catch (IllegalAccessException
                        | InvocationTargetException
                        | NoSuchFieldException e) {
                      e.printStackTrace();
                    }
                  });
        }
      }
      return dto;
    } catch (InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException
        | IntrospectionException e) {
      e.printStackTrace();
    }
    return null;
  }

  private <R> void applyMappers(T seed, R dto, PropertyDescriptor pd, PropertyDescriptor field) {
    AtomicBoolean mapped = new AtomicBoolean(false);
    Function mapper = find(pd.getPropertyType(), pd.getName());

    try {
      Object originalObject = pd.getReadMethod().invoke(seed);
      if (originalObject != null) {
        AtomicReference<Object> mappedObject = new AtomicReference<>();
        alreadyMapped.forEach(
            map -> {
              Object candidate = map.get(originalObject);
              if (candidate != null) {
                mappedObject.set(candidate);
              }
            });
        if (mappedObject.get() == null) {
          mappedObject.set(mapper.apply(originalObject));
        }
        field.getWriteMethod().invoke(dto, mappedObject.get());
        mapped.set(true);
      } else {
        // original value is not set so it can't be mapped. Ignore...
        mapped.set(true);
      }
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    }
    if (!mapped.get()) {
      log.warn(
          "Cannot convert {}({}) to {}({})",
          pd.getName(),
          pd.getPropertyType().getName(),
          field.getName(),
          field.getPropertyType().getName());
    }
  }
  //  private <R> void applyMappers(T seed, R dto, PropertyDescriptor pd, PropertyDescriptor field)
  // {
  //    AtomicBoolean mapped = new AtomicBoolean(false);
  //    mappers.forEach(
  //        (key, value) -> {
  //          if (pd.getPropertyType().equals(key)) {
  //            try {
  //              Object originalObject = pd.getReadMethod().invoke(seed);
  //              if (originalObject != null) {
  //                AtomicReference<Object> mappedObject = new AtomicReference<>();
  //                alreadyMapped.forEach(
  //                    map -> {
  //                      Object candidate = map.get(originalObject);
  //                      if (candidate != null) {
  //                        mappedObject.set(candidate);
  //                      }
  //                    });
  //                if (mappedObject.get() == null) {
  //                  mappedObject.set(value.apply(originalObject));
  //                }
  //                field.getWriteMethod().invoke(dto, mappedObject.get());
  //                mapped.set(true);
  //              } else {
  //                // original value is not set so it can't be mapped. Ignore...
  //                mapped.set(true);
  //              }
  //            } catch (IllegalAccessException e) {
  //              e.printStackTrace();
  //            } catch (InvocationTargetException e) {
  //              e.printStackTrace();
  //            }
  //          }
  //        });
  //    if (!mapped.get()) {
  //      log.warn(
  //          "Cannot convert {}({}) to {}({})",
  //          pd.getName(),
  //          pd.getPropertyType().getName(),
  //          field.getName(),
  //          field.getPropertyType().getName());
  //    }
  //  }

  public static class Hello {
    List<String> list = new ArrayList<>();
  }

  public static void main(String[] args)
      throws NoSuchFieldException, ClassNotFoundException, NoSuchMethodException,
          InvocationTargetException, InstantiationException, IllegalAccessException {
    DTOFactory<Address> addressDTO = new DTOFactory<>();
    Function<Customer, CustomerGET> customerMapper =
        customer -> {
          DTOFactory<Customer> dto = new DTOFactory<>();
          return dto.GET(customer, CustomerGET.class, List.of("firstname"));
        };
    addressDTO.registerMapper(Customer.class, customerMapper);
    Address address =
        Address.builder()
            .city("Bielefeld")
            .countryCode("DE")
            .customer(Customer.builder().firstname("Kostas").build())
            .build();
    final var addressGET = addressDTO.GET(address, AddressGET.class);
    System.out.println(addressGET.getCity());
    System.out.println(addressGET.getCountryCode());
    System.out.println(addressGET.getCustomer());

    final var stringType =
        ((ParameterizedType) Hello.class.getDeclaredField("list").getGenericType())
            .getActualTypeArguments()[0];
    System.out.println(stringType);
    Object obj = Class.forName(stringType.getTypeName()).getConstructor().newInstance();
    System.out.println(obj.getClass());
    System.out.println("END");
  }
}
