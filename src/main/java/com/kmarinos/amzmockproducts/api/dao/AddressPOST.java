package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class AddressPOST {

  boolean deliveryAddress;
  boolean invoiceAddress;
  boolean primaryAddress;
  @JsonInclude(Include.NON_EMPTY)
  String street;
  @JsonInclude(Include.NON_EMPTY)
  String streetNumber;
  @JsonInclude(Include.NON_EMPTY)
  String secondary;
  @JsonInclude(Include.NON_EMPTY)
  String zipCode;
  @JsonInclude(Include.NON_EMPTY)
  String city;
  @JsonInclude(Include.NON_EMPTY)
  String state;
  @JsonInclude(Include.NON_EMPTY)
  String countryCode;
}
