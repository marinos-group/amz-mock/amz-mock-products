package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.Address;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomerDTO {
  /********* POST ***********/

  public static Customer POST(CustomerPOST customerPOST) {
    return Customer.builder()
        .active(true)
        .firstname(customerPOST.getFirstname())
        .lastname(customerPOST.getLastname())
        .prefix(customerPOST.getPrefix())
        .suffix(customerPOST.getSuffix())
        .username(customerPOST.getUsername())
        .email(customerPOST.getEmail())
        .build();
  }

  /********* GET ***********/
  public static CustomerGET GET(Customer customer) {
    return GET(
        customer,
        buildAttributes(customer),
        addrList -> {
          if(addrList==null){
            return null;
          }
          return addrList.stream()
              .map(addr -> AddressDTO.buildAttributes(addr).build())
              .collect(Collectors.toList());
        });
  }

  public static List<CustomerGET> GET(List<Customer> customers) {
    return customers.stream().map(CustomerDTO::inCollection).collect(Collectors.toList());
  }

  private static CustomerGET GET(
      Customer customer,
      CustomerGET.CustomerGETBuilder customerGETBuilder,
      Function<Collection<Address>, List<AddressGET>> handleAddresses) {

    if (customer == null) {
      return null;
    }
    return customerGETBuilder.addresses(handleAddresses.apply(customer.getAddresses())).build();
  }

  public static CustomerGET.CustomerGETBuilder buildSimple(Customer customer) {
    if (customer == null) {
      return CustomerGET.builder();
    }
    return CustomerGET.builder()
        .id(customer.getId())
        .email(customer.getEmail())
        .username(customer.getUsername());
  }

  public static CustomerGET.CustomerGETBuilder buildAttributes(Customer customer) {
    return buildSimple(customer)
        .firstname(customer.getFirstname())
        .lastname(customer.getLastname())
        .prefix(customer.getPrefix())
        .suffix(customer.getSuffix());
  }

  public static CustomerGET inCollection(Customer customer) {
    CustomerGET.CustomerGETBuilder builder = buildSimple(customer);
    return GET(customer, builder, AddressDTO::referenced);
  }

  public static CustomerGET referenced(Customer customer) {
    return GET(customer, buildSimple(customer), dep -> null);
  }
}
