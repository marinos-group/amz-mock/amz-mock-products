package com.kmarinos.amzmockproducts.api.dao;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class SalesOrderItemPOST {

  ProductGET product;
  long quantity;

}
