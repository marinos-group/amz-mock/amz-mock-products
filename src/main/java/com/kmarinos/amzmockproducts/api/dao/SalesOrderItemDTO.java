package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.Product;
import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SalesOrderItemDTO {

  /********** POST ***********/
  /*public static SalesOrderItem POST(SalesOrderItemPOST salesOrderItemPOST){

    return SalesOrderItem.builder()

        .build();
  }*/

  /********** GET ***********/

  public static SalesOrderItemGET GET(SalesOrderItem salesOrderItem) {
    return GET(salesOrderItem, buildAttributes(salesOrderItem), SalesOrderDTO::referenced,ProductDTO::referenced);
  }

  public static List<SalesOrderItemGET> GET(List<SalesOrderItem> salesOrderItems) {
    return salesOrderItems.stream()
        .map(SalesOrderItemDTO::inCollection)
        .collect(Collectors.toList());
  }

  private static SalesOrderItemGET GET(
      SalesOrderItem salesOrderItem,
      SalesOrderItemGET.SalesOrderItemGETBuilder salesOrderItemGETBuilder,
      Function<SalesOrder, SalesOrderGET> handleSalesOrder,
      Function<Product, ProductGET> handleProduct) {

    if (salesOrderItem == null) {
      return null;
    }
    return salesOrderItemGETBuilder
        .salesOrder(handleSalesOrder.apply(salesOrderItem.getSalesOrder()))
        .product(handleProduct.apply(salesOrderItem.getProduct()))
        .build();
  }

  protected static SalesOrderItemGET.SalesOrderItemGETBuilder buildSimple(
      SalesOrderItem salesOrderItem) {
    if (salesOrderItem == null) {
      return SalesOrderItemGET.builder();
    }
    return SalesOrderItemGET.builder()
        .id(salesOrderItem.getId())
        .status(salesOrderItem.getStatus());
  }

  protected static SalesOrderItemGET.SalesOrderItemGETBuilder buildAttributes(
      SalesOrderItem salesOrderItem) {
    return buildSimple(salesOrderItem)
        .quantity(salesOrderItem.getQuantity())
        .pricePerItem(salesOrderItem.getPricePerItem())
        .price(salesOrderItem.getPrice());
  }

  protected static SalesOrderItemGET inCollection(SalesOrderItem salesOrderItem) {
    SalesOrderItemGET.SalesOrderItemGETBuilder builder = buildAttributes(salesOrderItem);
    return GET(salesOrderItem, builder, order->null,ProductDTO::referenced);
  }

  protected static SalesOrderItemGET referenced(SalesOrderItem salesOrderItem) {
    return GET(salesOrderItem, buildSimple(salesOrderItem), order -> null,product -> null);
  }

  protected static List<SalesOrderItemGET> referenced(Collection<SalesOrderItem> salesOrderItems) {
    if (salesOrderItems == null) {
      return null;
    }
    return salesOrderItems.stream().map(SalesOrderItemDTO::referenced).collect(Collectors.toList());
  }
}
