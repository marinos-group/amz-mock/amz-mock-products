package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.amzmockproducts.model.SalesOrderStatus;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class SalesOrderGET {

  String id;
  SalesOrderStatus status;
  CustomerGET customer;
  double invoicedAmount;
  AddressGET deliverTo;
  AddressGET invoiceTo;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  List<SalesOrderItemGET> orderItems;
}
