package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Department;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DepartmentDTO {

  public static DepartmentGET GET(Department department) {

    return GET(
        department,
        DepartmentDTO.buildAttributes(department),
        DepartmentDTO::referenced,
        DepartmentDTO::referenced);
  }

  public static List<DepartmentGET> GET(List<Department> departments) {
    return departments.stream()
        .map(dep -> GET(dep, buildAttributes(dep), DepartmentDTO::referenced, subDep -> null))
        .collect(Collectors.toList());
  }

  private static DepartmentGET GET(
      Department department,
      DepartmentGET.DepartmentGETBuilder departmentGETBuilder,
      Function<Department, DepartmentGET> handleParent,
      Function<Collection<Department>, List<DepartmentGET>> handleSubDepartments) {

    if (department == null) {
      return null;
    }
    return departmentGETBuilder
        .parent(handleParent.apply(department.getParent()))
        .subDepartments(handleSubDepartments.apply(department.getSubDepartments().values()))
        .build();
  }

  public static DepartmentGET.DepartmentGETBuilder buildSimple(Department department) {
    if (department == null) {
      return DepartmentGET.builder();
    }
    return DepartmentGET.builder().id(department.getId()).name(department.getName());
  }

  public static DepartmentGET.DepartmentGETBuilder buildAttributes(Department department) {
    return DepartmentDTO.buildSimple(department)
        .bestsellerUrl(department.getBestsellerUrl())
        .category(department.getCategory());
  }

  public static DepartmentGET referenced(Department department) {
    return GET(
        department, buildSimple(department), parent -> null, subDepartments -> null);
  }

  public static List<DepartmentGET> referenced(Collection<Department> departments) {
    return departments.stream().map(DepartmentDTO::referenced).collect(Collectors.toList());
  }
  static Function<Department,DepartmentGET> departmentWithCategory = department -> {
    DepartmentGET dGET = DepartmentDTO.referenced(department);
    dGET.setCategory(department.getCategory());
    return dGET;
  };
}
