package com.kmarinos.amzmockproducts.api.dao;

import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class SalesOrderPOST {

  LocalDateTime submitOrderAt;
  List<SalesOrderItemPOST> items;
}
