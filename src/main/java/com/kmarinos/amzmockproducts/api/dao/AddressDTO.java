package com.kmarinos.amzmockproducts.api.dao;

import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class AddressDTO {

  /********** POST ***********/
  public static Address POST(AddressPOST addressPOST){

    return Address.builder()
        .street(addressPOST.getStreet())
        .streetNumber(addressPOST.getStreetNumber())
        .secondary(addressPOST.getSecondary())
        .city(addressPOST.getCity())
        .state(addressPOST.getState())
        .zipCode(addressPOST.getZipCode())
        .countryCode(addressPOST.getCountryCode())
        .primaryAddress(addressPOST.isPrimaryAddress())
        .invoiceAddress(addressPOST.isInvoiceAddress())
        .deliveryAddress(addressPOST.isDeliveryAddress())
        .build();
  }

  /********** GET ***********/

  public static AddressGET GET(Address address) {
    return GET(address, buildAttributes(address), CustomerDTO::referenced);
  }

  public static List<AddressGET> GET(List<Address> addresses) {
    return addresses.stream()
        .map(AddressDTO::inCollection)
        .collect(Collectors.toList());
  }

  private static AddressGET GET(
      Address address,
      AddressGET.AddressGETBuilder addressGETBuilder,
      Function<Customer, CustomerGET> handleCustomer) {

    if (address == null) {
      return null;
    }
    return addressGETBuilder.customer(handleCustomer.apply(address.getCustomer())).build();
  }

  protected static AddressGET.AddressGETBuilder buildSimple(Address address) {
    if (address == null) {
      return AddressGET.builder();
    }
    return AddressGET.builder()
        .id(address.getId())
        .deliveryAddress(address.isDeliveryAddress())
        .invoiceAddress(address.isInvoiceAddress())
        .primaryAddress(address.isPrimaryAddress());
  }

  protected static AddressGET.AddressGETBuilder buildAttributes(Address address) {
    return buildSimple(address)
        .street(address.getStreet())
        .streetNumber(address.getStreetNumber())
        .secondary(address.getSecondary())
        .zipCode(address.getZipCode())
        .city(address.getCity())
        .state(address.getState())
        .countryCode(address.getCountryCode());
  }

  protected static AddressGET inCollection(Address address){
    AddressGET.AddressGETBuilder builder = buildAttributes(address);
    return GET(address, builder, CustomerDTO::referenced);

  }
  protected static AddressGET referenced(Address address) {
    return GET(address, buildSimple(address), cus -> null);
  }
  protected static List<AddressGET> referenced(Collection<Address> addresses){
    if(addresses==null){
      return null;
    }
    return addresses.stream().map(AddressDTO::referenced).collect(Collectors.toList());
  }
}
