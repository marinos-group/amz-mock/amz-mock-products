package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class DepartmentGET {

  String id;
  String name;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  String bestsellerUrl;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  String category;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  DepartmentGET parent;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  List<DepartmentGET>subDepartments;

}
