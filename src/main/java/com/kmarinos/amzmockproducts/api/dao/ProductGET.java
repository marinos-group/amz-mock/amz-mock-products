package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.amzmockproducts.model.ProductRating;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class ProductGET {

  String id;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  ProductRating rating;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  DepartmentGET department;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  Double price;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  String url;
  String descriptionShort;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  String descriptionLong;

}
