package com.kmarinos.amzmockproducts.api.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class CustomerPOST {

  @JsonInclude(Include.NON_EMPTY)
  String firstname;
  @JsonInclude(Include.NON_EMPTY)
  String lastname;
  @JsonInclude(Include.NON_EMPTY)
  String prefix;
  @JsonInclude(Include.NON_EMPTY)
  String suffix;
  String username;
  String email;
  @JsonInclude(Include.NON_EMPTY)
  AddressPOST address;

}
