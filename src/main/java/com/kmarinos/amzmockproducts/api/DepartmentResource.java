package com.kmarinos.amzmockproducts.api;

import com.kmarinos.amzmockproducts.api.dao.DepartmentDTO;
import com.kmarinos.amzmockproducts.api.dao.DepartmentGET;
import com.kmarinos.amzmockproducts.service.DepartmentService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class DepartmentResource {

  private final DepartmentService departmentService;

  @GetMapping("categories")
  public List<String> fetchAllCategories(){
    return departmentService.getAllCategories();
  }
  @GetMapping("departments")
  public List<DepartmentGET> fetchAllDepartments(){
    return DepartmentDTO.GET(departmentService.getAllDepartments().stream().filter(d->d.getParent()!=null).limit(10).collect(
        Collectors.toList()));
  }
  @GetMapping("departments/{id}")
  public DepartmentGET fetchDepartmentWithId(@PathVariable("id") String id){
    return DepartmentDTO.GET(departmentService.getDepartmentById(id));
  }
}
