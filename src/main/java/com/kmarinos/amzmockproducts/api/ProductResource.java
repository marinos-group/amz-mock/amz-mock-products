package com.kmarinos.amzmockproducts.api;

import com.kmarinos.amzmockproducts.api.dao.ProductDTO;
import com.kmarinos.amzmockproducts.api.dao.ProductGET;
import com.kmarinos.amzmockproducts.service.ProductService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProductResource {

  private final ProductService productService;

  @GetMapping("products")
  public List<ProductGET> getAllProducts(){
    return ProductDTO.GET(productService.getAllProducts(100));
  }
  @GetMapping("products/{id}")
  public ProductGET fetchProductsWithId(@PathVariable("id") String id){
    return ProductDTO.GET(productService.getProductById(id));
  }
  @GetMapping("/browse")
  public List<ProductGET>getRandomProducts(@RequestParam("c")String category){
    return ProductDTO.GET(productService.getRandomProductsInCategory(category,50));
  }

}
