package com.kmarinos.amzmockproducts.api;

import com.kmarinos.amzmockproducts.api.dao.SalesOrderDTO;
import com.kmarinos.amzmockproducts.api.dao.SalesOrderGET;
import com.kmarinos.amzmockproducts.api.dao.SalesOrderPOST;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.service.AddressService;
import com.kmarinos.amzmockproducts.service.CustomerService;
import com.kmarinos.amzmockproducts.service.ProductService;
import com.kmarinos.amzmockproducts.service.SalesOrderService;
import com.kmarinos.amzmockproducts.service.process.ProcessingQueue;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("api")
public class SalesOrderResource {

  private final SalesOrderService salesOrderService;
  private final CustomerService customerService;
  private final AddressService addressService;
  private final ProductService productService;
  @Qualifier("salesOrderQueue")
  private final ProcessingQueue processingQueue;
  @GetMapping("customers/{id}/sales-orders")
  public List<SalesOrderGET> getSalesOrdersForCustomerById(@PathVariable("id")String customerId){
    Customer customer = customerService.getCustomerById(customerId);
    return SalesOrderDTO.GET(salesOrderService.getAllSalesForCustomer(customer));
  }
  @GetMapping("sales-orders/{id}")
  public SalesOrderGET getSalesOrderById(@PathVariable("id")String orderId){
    return SalesOrderDTO.GET(salesOrderService.getSalesOrderById(orderId));
  }
  @PostMapping("customers/{id}/buy")
  public ResponseEntity<Void> buyItems(@PathVariable("id")String customerId,@RequestBody SalesOrderPOST salesOrderPOST){

    processingQueue.process(()->{
      Customer customer = customerService.getCustomerById(customerId,addressService::getAddressesByCustomer);
      salesOrderService.registerOrder(salesOrderPOST,customer,productService::getProductById);
    });

    return ResponseEntity.status(201).build();
  }
}
