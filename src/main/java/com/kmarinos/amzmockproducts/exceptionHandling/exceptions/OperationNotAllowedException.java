package com.kmarinos.amzmockproducts.exceptionHandling.exceptions;

public class OperationNotAllowedException extends RuntimeException{
    public OperationNotAllowedException(String message){
        super(message);
    }
}
