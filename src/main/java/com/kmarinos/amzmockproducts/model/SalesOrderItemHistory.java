package com.kmarinos.amzmockproducts.model;

import com.kmarinos.amzmockproducts.model.listeners.SalesOrderHistoryListener;
import com.kmarinos.amzmockproducts.model.listeners.SalesOrderItemHistoryListener;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Immutable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "sales_order_item_H")
@Immutable
@EntityListeners(SalesOrderItemHistoryListener.class)
public class SalesOrderItemHistory {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Include
  String id;

  @Column(nullable = false, name = "sales_order_item_id")
  String salesOrderItemId;

  @Transient
  @Setter(AccessLevel.NONE)
  SalesOrderItem orderItem;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private int status;

  @CreatedDate
  @Column(nullable = false, updatable = false)
  @Builder.Default
  LocalDateTime createdAt = LocalDateTime.now();

  public SalesOrderItemStatus getStatus() {
    return SalesOrderItemStatus.parse(this.status);
  }

  public void setStatus(SalesOrderItemStatus status) {
    this.status = status.asValue();
  }

  public void setOrderItem(SalesOrderItem orderItem) {
    this.orderItem = orderItem;
    this.salesOrderItemId = orderItem.getId();
  }

  public static class SalesOrderItemHistoryBuilder {
    public SalesOrderItemHistory.SalesOrderItemHistoryBuilder status(SalesOrderItemStatus status) {
      this.status = status.asValue();
      return this;
    }

    public SalesOrderItemHistory.SalesOrderItemHistoryBuilder orderItem(SalesOrderItem orderItem) {
      this.orderItem = orderItem;
      this.salesOrderItemId = orderItem.getId();
      return this;
    }
  }
}
