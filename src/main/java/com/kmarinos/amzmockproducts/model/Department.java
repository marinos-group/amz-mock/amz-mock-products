package com.kmarinos.amzmockproducts.model;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "departments")
public class Department {
  String name;
  @Column(length=2000)
  String bestsellerUrl;
  String category;
  @Id @Include String id;
  @Exclude @OneToOne Department parent;
  @Transient @Default Map<String, Department> subDepartments = new HashMap<>();

  public Stream<Department> streamSubDepartments() {
    return Stream.concat(
        Stream.of(this),
        subDepartments.values().stream().flatMap(Department::streamSubDepartments));
  }
}
