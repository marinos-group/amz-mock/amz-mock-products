package com.kmarinos.amzmockproducts.model;

import com.kmarinos.amzmockproducts.model.SalesOrder.SalesOrderBuilder;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "sales_order_items")
public class SalesOrderItem {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  String id;

  @ManyToOne
  SalesOrder salesOrder;
  @ManyToOne Product product;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  int status;

  long quantity;
  double pricePerItem;
  double price;

  @CreatedDate
  @Column(nullable = false, updatable = false)
  @Builder.Default
  LocalDateTime createdAt = LocalDateTime.now();

  @LastModifiedDate
  @Builder.Default LocalDateTime updatedAt = LocalDateTime.now();

  public SalesOrderItemStatus getStatus() {
    return SalesOrderItemStatus.parse(this.status);
  }

  public void setStatus(SalesOrderItemStatus status) {
    this.status = status.asValue();
  }

  public static class SalesOrderItemBuilder {
    public SalesOrderItem.SalesOrderItemBuilder status(SalesOrderItemStatus status) {
      this.status = status.asValue();
      return this;
    }
  }
}
