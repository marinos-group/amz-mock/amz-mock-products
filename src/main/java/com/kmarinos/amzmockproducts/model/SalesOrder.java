package com.kmarinos.amzmockproducts.model;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "sales_orders")
public class SalesOrder {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  String id;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private int status;

  @ManyToOne Customer customer;
  double invoicedAmount;
  @ManyToOne
  Address deliverTo;
  @ManyToOne
  Address invoiceTo;
  @CreatedDate
  @Column(nullable = false, updatable = false)
  @Builder.Default
  LocalDateTime createdAt = LocalDateTime.now();

  @LastModifiedDate @Builder.Default LocalDateTime updatedAt = LocalDateTime.now();
  @Transient
  List<SalesOrderItem>salesOrderItems;

  public SalesOrderStatus getStatus() {
    return SalesOrderStatus.parse(this.status);
  }

  public void setStatus(SalesOrderStatus status) {
    this.status = status.asValue();
  }

  public static class SalesOrderBuilder {
    public SalesOrderBuilder status(SalesOrderStatus status) {
      this.status = status.asValue();
      return this;
    }
  }
}
