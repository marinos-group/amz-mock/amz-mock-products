package com.kmarinos.amzmockproducts.model;

public enum SalesOrderStatus {
  OPEN(10),APPROVED(20),IN_WORK(40),CLOSED(90);
  private final int value;
  SalesOrderStatus(int value){
    this.value=value;
  }
  public int asValue(){
    return value;
  }public static SalesOrderStatus parse(int value) {
    SalesOrderStatus status = null;
    for (SalesOrderStatus item : SalesOrderStatus.values()) {
      if (item.asValue()==value) {
        status = item;
        break;
      }
    }
    return status;
  }

}
