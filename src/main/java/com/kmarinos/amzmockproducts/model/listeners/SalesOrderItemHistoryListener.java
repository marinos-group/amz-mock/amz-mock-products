package com.kmarinos.amzmockproducts.model.listeners;


import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import com.kmarinos.amzmockproducts.model.SalesOrderItemHistory;
import com.kmarinos.amzmockproducts.repository.SalesOrderItemRepository;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class SalesOrderItemHistoryListener {
  @Autowired
  public void init(SalesOrderItemRepository salesOrderItemRepository){
    SalesOrderItemHistoryListener.salesOrderItemRepository=salesOrderItemRepository;
  }
  private static SalesOrderItemRepository salesOrderItemRepository;

  @PrePersist
  void afterUpdate(SalesOrderItemHistory history) {
    SalesOrderItem orderItem= history.getOrderItem();
    if(history.getSalesOrderItemId()!=null){
      orderItem=salesOrderItemRepository.findById(history.getSalesOrderItemId()).orElse(history.getOrderItem());
    }
    orderItem.setUpdatedAt(LocalDateTime.now());
    orderItem.setStatus(history.getStatus());
    history.setOrderItem(salesOrderItemRepository.saveAndFlush(orderItem));

  }
  @PostLoad
  void afterLoad(SalesOrderItemHistory history){
    history.setOrderItem(Objects.requireNonNull(
        salesOrderItemRepository.findById(history.getSalesOrderItemId()).orElse(null)));
  }
}
