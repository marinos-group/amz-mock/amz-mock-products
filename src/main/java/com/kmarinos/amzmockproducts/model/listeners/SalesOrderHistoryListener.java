package com.kmarinos.amzmockproducts.model.listeners;


import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderHistory;
import com.kmarinos.amzmockproducts.repository.SalesOrderRepository;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.transaction.Transactional;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class SalesOrderHistoryListener {
  @Autowired
  public void init(SalesOrderRepository salesOrderRepository){
    SalesOrderHistoryListener.salesOrderRepository=salesOrderRepository;
  }
  private static SalesOrderRepository salesOrderRepository;

  @PrePersist
  void afterUpdate(SalesOrderHistory history) {
    SalesOrder order= history.getOrder();
    if(history.getSalesOrderId()!=null){
      order=salesOrderRepository.findById(history.getSalesOrderId()).orElse(history.getOrder());
    }
    order.setUpdatedAt(LocalDateTime.now());
    order.setStatus(history.getStatus());
    history.setOrder(salesOrderRepository.saveAndFlush(order));
  }
  @PostLoad
  void afterLoad(SalesOrderHistory history){
    history.setOrder(Objects.requireNonNull(
        salesOrderRepository.findById(history.getSalesOrderId()).orElse(null)));
  }
}
