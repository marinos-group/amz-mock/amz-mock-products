package com.kmarinos.amzmockproducts.model;

import com.kmarinos.amzmockproducts.model.SalesOrder.SalesOrderBuilder;
import com.kmarinos.amzmockproducts.model.listeners.SalesOrderHistoryListener;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Immutable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "sales_order_H")
@Immutable
@EntityListeners(SalesOrderHistoryListener.class)
public class SalesOrderHistory {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Include
  String id;

  @Column(nullable = false, name = "sales_order_id")
  String salesOrderId;

  @Transient
  @Setter(AccessLevel.NONE)
  SalesOrder order;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private int status;

  @CreatedDate
  @Column(nullable = false, updatable = false)
  @Builder.Default
  LocalDateTime createdAt = LocalDateTime.now();

  public SalesOrderStatus getStatus() {
    return SalesOrderStatus.parse(this.status);
  }

  public void setStatus(SalesOrderStatus status) {
    this.status = status.asValue();
  }

  public void setOrder(SalesOrder order) {
    this.order = order;
    this.salesOrderId = order.getId();
  }

  public static class SalesOrderHistoryBuilder {
    public SalesOrderHistory.SalesOrderHistoryBuilder status(SalesOrderStatus status) {
      this.status = status.asValue();
      return this;
    }

    public SalesOrderHistory.SalesOrderHistoryBuilder order(SalesOrder order) {
      this.order = order;
      this.salesOrderId = order.getId();
      return this;
    }
  }
}
