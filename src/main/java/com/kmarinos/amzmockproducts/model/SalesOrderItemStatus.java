package com.kmarinos.amzmockproducts.model;

public enum SalesOrderItemStatus {
  OPEN(10),APPROVED(20),PAID(30),PICKED(41),IN_TRANSIT(49),DELIVERED(50),CLOSED(90);
  private final int value;
  SalesOrderItemStatus(int value){
    this.value=value;
  }
  public int asValue(){
    return value;
  }public static SalesOrderItemStatus parse(int value) {
    SalesOrderItemStatus status = null;
    for (SalesOrderItemStatus item : SalesOrderItemStatus.values()) {
      if (item.asValue()==value) {
        status = item;
        break;
      }
    }
    return status;
  }

}
