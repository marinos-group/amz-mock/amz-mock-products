package com.kmarinos.amzmockproducts.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="products")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Product {
  @Id @Include String id;
  @ManyToOne Department department;
  @Embedded ProductRating rating;
  Double price;
  @Column(length=2000)
  String url;
  @Column(length=2000)
  String imgUrl;
  @Column(length=2000)
  String descriptionShort;
  @Column(length=5000)
  String descriptionLong;
}
