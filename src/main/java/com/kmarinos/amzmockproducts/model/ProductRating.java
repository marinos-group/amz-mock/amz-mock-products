package com.kmarinos.amzmockproducts.model;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class ProductRating {

  Double stars;
  Double totalStars;
  Long totalRatings;

  //Str: {stars} out of {totalStars} stars
  public static ProductRating parseFromString(String str, long totalRatings){
    if(!(str.contains(" out of ")&&str.endsWith("stars"))){
      return null;
    }
    var stars = Double.parseDouble(str.split(" ")[0]);
    var totalStars = Double.parseDouble(str.split(" ")[3]);
    return ProductRating.builder()
        .totalRatings(totalRatings)
        .stars(stars)
        .totalStars(totalStars)
        .build();
  }
}
