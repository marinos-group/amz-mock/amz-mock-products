package com.kmarinos.amzmockproducts;

import com.kmarinos.amzmockproducts.api.dao.AddressGET;
import com.kmarinos.amzmockproducts.api.dao.CustomerGET;
import com.kmarinos.amzmockproducts.api.dao.DTOFactory;
import com.kmarinos.amzmockproducts.api.dao.DTOFactory.Hello;
import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.ToString.Exclude;

public class TestDTOFactory {

  public static void main(String[] args) {
    Parent parent =
        Parent.builder()
            .name("Papa")
            .child(Child.builder().name("Ben").lastPresent("Cookies").build())
            .child2(Child.builder().name("Johanna").lastPresent("Doll").build())
            .grandchildren(
                List.of(Child.builder().name("Toto").lastPresent("Car").build(), Child.builder().name("Bobo").lastPresent("Psychological trauma").build()))
            .grandchildren2(
                List.of(Child.builder().name("Popo").build(), Child.builder().name("Pipi").build()))
            .info(List.of("Hello", "world"))
            .items(List.of(Item.builder().name("Shoes").build(), Item.builder().name("Hat").build()))
            .build();
    parent.getChild().setParent(parent);
    parent.getChild2().setParent(parent);
    parent.getGrandchildren().forEach(c->c.setParent(parent));
    parent.getItems().forEach(i->i.setParent(parent));
    DTOFactory<Parent> dtoParent = new DTOFactory<>();
    Function<Child,ChildGET>mapChild=child -> {
      DTOFactory<Child> dtoChild = new DTOFactory<>();
      dtoChild.registerMapper(
          Parent.class,
          parent2 -> {
            return dtoParent.GET(parent2, ParentGET.class);
          });
      return dtoChild.GET(child, ChildGET.class,List.of("name","parent"));
    };
    Function<Child,ChildGET>mapGrandchild=child -> {
      DTOFactory<Child> dtoChild = new DTOFactory<>();
      dtoChild.registerMapper(
          Parent.class,
          parent2 -> {
            return dtoParent.GET(parent2, ParentGET.class);
          });
      return dtoChild.GET(child, ChildGET.class);
    };
    Function<Item,ItemGET>mapItem=item -> {
      DTOFactory<Item> dtoItem = new DTOFactory<>();
      dtoItem.registerMapper(
          Parent.class,
          parent2 -> {
            return dtoParent.GET(parent2, ParentGET.class);
          });
      return dtoItem.GET(item, ItemGET.class);
    };
    dtoParent.registerMapper(
        Child.class,
        mapChild);
    dtoParent.registerMapper(List.class,list->{
      return list.stream().map(mapGrandchild).collect(Collectors.toList());
    });
    dtoParent.registerMapper(List.class,"items", list->{
      return list.stream().map(mapItem).collect(Collectors.toList());
    });

    final var parentGET = dtoParent.GET(parent, ParentGET.class);
    System.out.println(parentGET.getChild());
    System.out.println(parentGET.getChild2());
    System.out.println(
    parentGET
        .getGrandchildren());
    System.out.println(
        parentGET
            .getGrandchildren2());
    System.out.println(parentGET.getInfo());
    System.out.println(parentGET.getItems());

    //    try {
    //      main1();
    //    } catch (NoSuchFieldException
    //        | ClassNotFoundException
    //        | NoSuchMethodException
    //        | InvocationTargetException
    //        | InstantiationException
    //        | IllegalAccessException e) {
    //      e.printStackTrace();
    //    }
  }

  public static void main1()
      throws NoSuchFieldException, ClassNotFoundException, NoSuchMethodException,
          InvocationTargetException, InstantiationException, IllegalAccessException {
    DTOFactory<Address> addressDTO = new DTOFactory<>();
    Function<Customer, CustomerGET> customerMapper =
        customer -> {
          DTOFactory<Customer> dto = new DTOFactory<>();
          return dto.GET(customer, CustomerGET.class, List.of("firstname"));
        };
    addressDTO.registerMapper(Customer.class, customerMapper);
    Address address =
        Address.builder()
            .city("Bielefeld")
            .countryCode("DE")
            .customer(Customer.builder().firstname("Kostas").build())
            .build();
    final var addressGET = addressDTO.GET(address, AddressGET.class);
    System.out.println(addressGET.getCity());
    System.out.println(addressGET.getCountryCode());
    System.out.println(addressGET.getCustomer());

    final var stringType =
        ((ParameterizedType) Hello.class.getDeclaredField("list").getGenericType())
            .getActualTypeArguments()[0];
    System.out.println(stringType);
    Object obj = Class.forName(stringType.getTypeName()).getConstructor().newInstance();
    System.out.println(obj.getClass());
    System.out.println("END");
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class Parent {
    @Getter(AccessLevel.NONE)
    String name;

    @ToString.Exclude Child child;
    @ToString.Exclude Child child2;
    List<Child> grandchildren;
    List<Child> grandchildren2;
    List<String> info;
    List<Item> items;

    public String getName() {
      return "Parent: " + name;
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class Child {
    @Getter(AccessLevel.NONE)
    String name = "";
    String lastPresent;

    @ToString.Exclude Parent parent;

    public String getName() {
      return "Child: " + name;
    }
  }
  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class Item {
    @Getter(AccessLevel.NONE)
    String name = "";

    @ToString.Exclude Parent parent;

    public String getName() {
      return "Item: " + name;
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class ParentGET {
    @Getter(AccessLevel.NONE)
    String name;

    @ToString.Exclude ChildGET child;
    @ToString.Exclude ChildGET child2;

    List<ChildGET> grandchildren;
    List<ChildGET> grandchildren2;
    List<String> info;
    List<ItemGET> items;

    public String getName() {
      return "ParentGET: " + name;
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class ChildGET {
    @Getter(AccessLevel.NONE)
    String name = "";
    String lastPresent;

    @ToString.Exclude ParentGET parent;

    public String getName() {
      return "ChildGET: " + name;
    }
  }
  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  public static class ItemGET {
    @Getter(AccessLevel.NONE)
    String name = "";

    @ToString.Exclude ParentGET parent;

    public String getName() {
      return "ItemGET: " + name;
    }
  }
}
