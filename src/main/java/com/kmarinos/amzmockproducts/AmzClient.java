package com.kmarinos.amzmockproducts;

import com.gargoylesoftware.htmlunit.FrameContentHandler;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.BaseFrameElement;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener;
import com.kmarinos.amzmockproducts.model.Department;
import com.kmarinos.amzmockproducts.model.Product;
import com.kmarinos.amzmockproducts.model.ProductRating;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AmzClient {

  private final WebClient webClient;
  private final String ROOT_URL = "https://www.amazon.de";
  private final String DEPARTMENT_DISPLAY_NAME_PREFIX = "Best Sellers in ";
  private final UrlValidator urlValidator = new UrlValidator();

  public AmzClient() {
    webClient = new WebClient();
    this.configureClient();
  }

  public List<Product> fetchAllProducts(Department department,int retry){
    for(int i = 0;i<retry;i++){
      List<Product> products =fetchAllProducts(department);
      if(products.size()>0){
        return products;
      }else{
//        log.warn("Returned no results. Retry {}/{}",i,retry);
        try {
          TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    return new ArrayList<>();
  }

  public List<Product> fetchAllProducts(Department department) {
    HtmlPage page =
        this.fetchRelativePage(department.getBestsellerUrl());

    List<Product> products= new ArrayList<>();

    List<DomElement> cards= page.getElementsById("gridItemRoot");
    if (cards.size() == 0) {
      cards = page.getByXPath("//li[@class='zg-item-immersion']");
    }
    if(cards.size()==0){
      return new ArrayList<>();
    }
    for (DomElement itemContainer : cards) {
      DomNodeList<HtmlElement> spans = itemContainer.getElementsByTagName("span");
      /* Spans-List element lookup
      0: ranking in bestseller (#2)
      1: long description (SONAX Anti-Frost and Clear View Concentrate)
      2: rating (4.7 out of 5 stars)
      3: total reviews (459)
      4: price range (€3.98 - €24.44)
      5: smallest price (€3.98)
      6: biggest price (€24.44)
       */
      if(spans.size()<2){
        continue;
      }
      String parsedHref=((HtmlElement) spans.get(1).getParentNode()).getAttribute("href");
      String url = cleanUrl(parsedHref);
      if(!url.contains("/dp/")){
        continue;
      }
      String descriptionShort="";
      if(parsedHref.startsWith("/-/en/")){
        descriptionShort=url.split("/-/en/")[1].split("/dp")[0];
      }else if(parsedHref.startsWith(ROOT_URL)){
        descriptionShort=url.replace(ROOT_URL+"/","").split("/")[0];
      }
      descriptionShort=descriptionShort.replace("-"," ");
      String id=url.split("/dp/")[1];
      String descriptionLong = spans.get(1).getTextContent().replace("\u00a0","");
      String imgUrl=itemContainer.getElementsByTagName("img").stream().map(el->cleanUrl(el.getAttribute("src"))).findFirst().orElse("");
      ProductRating rating=null;
      double price=0.0;
      if(spans.size()>2){
        if(spans.get(2).getTextContent().endsWith("stars")){
          rating=ProductRating.parseFromString(spans.get(2).getTextContent(),Long.parseLong(spans.get(3).getTextContent().replace(",","")));
        }
        try{
          if (spans.size() == 5) {
            if(spans.get(4).getTextContent().contains("€")){
              price = Double.parseDouble(spans.get(4).getTextContent().replace("€", "").replace(",",""));
            }
          }else if(spans.size() == 7) {
            if(spans.get(5).getTextContent().contains("€")){
              price = Double.parseDouble(spans.get(5).getTextContent().replace("€", "").replace(",",""));
            }
          }
        }catch(Exception ignored){
        }
      }
      products.add(Product.builder()
              .descriptionShort(descriptionShort)
              .descriptionLong(descriptionLong)
              .imgUrl(imgUrl)
              .rating(rating)
              .id(id)
              .url(url)
              .price(price)
              .department(department)
          .build());
    }
    return products;
  }

  public void fetchAllDepartments(Consumer<Department> postProcess) {
    HtmlPage page = fetchRelativePage("/-/en/gp/bestsellers/");
    String displayName = this.getDepartmentRealTitle(page);
    List<Department> topDepartments =
        page.getElementsByTagName("div").stream()
            .filter(d -> "treeitem".equals(d.getAttribute("role")))
            .filter(
                d ->
                    d.getChildElementCount() == 1
                        && d.getFirstElementChild().getTagName().equals("a"))
            .map(
                d ->
                    Department.builder()
                        .name(
                            displayName.isEmpty()
                                ? d.getFirstElementChild().getTextContent()
                                : displayName)
                        .bestsellerUrl(d.getFirstElementChild().getAttribute("href"))
                        .build())
            .collect(Collectors.toList());
    int count = 0;
    for (Department dep : topDepartments) {
      fetchSubDepartments("", dep);
      postProcess.accept(dep);
      count++;
      log.info("Parsed Department {}/{}", count, topDepartments.size());
    }
    List<Department> flatDepartments =
        topDepartments.stream()
            .flatMap(Department::streamSubDepartments)
            .collect(Collectors.toList());

    log.info("Total departments:{}", flatDepartments.size());
  }

  public void fetchAllDepartments() {
    this.fetchAllDepartments(department -> {});
  }

  private void fetchSubDepartments(String category, Department parent) {
    HtmlPage page = this.fetchRelativePage(parent.getBestsellerUrl());

    String realTitle = this.getDepartmentRealTitle(page);
    String[] processedUrl = getDepartmentIdAndCategoryFromUrl(page.getUrl().toString());
    Department subDepartment =
        Department.builder()
            .name(realTitle)
            .category(processedUrl[0])
            .id(processedUrl[1])
            .bestsellerUrl(page.getUrl().toString())
            .build();

    parent.getSubDepartments().put(subDepartment.getName(), subDepartment);
    subDepartment.setParent(parent);

    log.debug("Added {}", subDepartment);

    List<DomElement> navItems =
        page.getElementsByTagName("div").stream()
            .filter(d -> "treeitem".equals(d.getAttribute("role")))
            .filter(d -> d.getChildElementCount() == 1)
            .collect(Collectors.toList());
    boolean foundParent = false;
    boolean shouldTraverse = false;
    for (DomElement element : navItems) {
      String displayText = element.getFirstElementChild().getTextContent();
      if (displayText.equals("Any Department")) {
        continue;
      }
      if (displayText.equals(subDepartment.getName())) {
        DomElement sibling = element.getNextElementSibling();
        foundParent = true;
        if (sibling != null
            && sibling.hasAttribute("class")
            && sibling.getAttribute("class").contains("browse-group")) {
          shouldTraverse = true;
        }
      } else {
        if (foundParent && shouldTraverse) {
          String fixUrl = element.getFirstElementChild().getAttribute("href");
          if (fixUrl != null && !fixUrl.isEmpty()) {
            subDepartment.setBestsellerUrl(element.getFirstElementChild().getAttribute("href"));
          }
          fetchSubDepartments(subDepartment.getCategory(), subDepartment);
        }
      }
    }
  }

  public String getTestPageTitle() {
    HtmlPage page = fetchPage("https://www.amazon.de/-/en/gp/bestsellers/");
    return page.getTitleText();
  }

  private String cleanUrl(String url){
    if(!urlValidator.isValid(url)){
      url=ROOT_URL+url;
    }
    return url.split("/ref=")[0];
  }
  private String[] getDepartmentIdAndCategoryFromUrl(String url) {
    if (url.equals("https://www.amazon.de/")) {
      System.out.println("Cleaning " + url);
      return new String[] {"", ""};
    }
    String cleanedUpUrl = url.split("/ref=")[0].split("bestsellers/")[1];
    String[] parts = cleanedUpUrl.split("/");
    String id = "";
    String category = "";
    category = parts[0];
    if (parts.length > 1) {
      id = parts[1];
    }
    return new String[] {category, id};
  }

  private HtmlPage fetchPage(String url) {
    HtmlPage page = null;
    try {
      TimeUnit.MILLISECONDS.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      page = webClient.getPage(url);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return page;
  }

  private String getDepartmentRealTitle(HtmlPage page) {
    return page.getElementsByTagName("h1").stream()
        .filter(
            h ->
                h.hasAttribute("class")
                    && h.getTextContent().startsWith(DEPARTMENT_DISPLAY_NAME_PREFIX))
        .map(DomNode::getTextContent)
        .map(s -> s.replace(DEPARTMENT_DISPLAY_NAME_PREFIX, ""))
        .findFirst()
        .orElse("");
  }

  private HtmlPage fetchRelativePage(String url) {
    if (url == null || url.isEmpty()) {
      return null;
    }
    if (url.contains(ROOT_URL)) {
      return fetchPage(url);
    }
    return fetchPage(ROOT_URL + url);
  }

  private void configureClient() {
    webClient.waitForBackgroundJavaScript(15000);
    webClient.waitForBackgroundJavaScriptStartingBefore(5000);
//    webClient.setAjaxController(new NicelyResynchronizingAjaxController());
    webClient.getOptions().setUseInsecureSSL(true);
    webClient.getOptions().setJavaScriptEnabled(false);
    webClient.setCssErrorHandler(new SilentCssErrorHandler());
    webClient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());
    webClient.setFrameContentHandler(
        new FrameContentHandler() {
          @Override
          public boolean loadFrameDocument(BaseFrameElement baseFrameElement) {
            return false;
          }
        });
  }
}
