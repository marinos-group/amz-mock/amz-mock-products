package com.kmarinos.amzmockproducts.service;

import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.amzmockproducts.model.Department;
import com.kmarinos.amzmockproducts.model.Product;
import com.kmarinos.amzmockproducts.model.ProductRating;
import com.kmarinos.amzmockproducts.repository.ProductRepository;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

  private final ProductRepository productRepository;
  private final ServiceUtils serviceUtils;

  @Value("${import.products.file:exchange/products_master.csv}")
  private String csvImportFile;

  public List<Product> getAllProducts() {
    return productRepository.findAll();
  }
  @Transactional
  public List<Product> getAllProducts(long limit) {
    return productRepository.findAllProducts().limit(limit).collect(Collectors.toList());
  }

  public Product getProductById(String id) {
    if (id == null) {
      throw new EntityNotFoundException(Product.class, "id", null);
    }
    return productRepository
        .findById(id)
        .orElseThrow(() -> new EntityNotFoundException(Product.class, "id", id));
  }
  public List<Product>getRandomProductsInCategory(String category, int amount){
    long qty = productRepository.countProductsByDepartment_Category(category);
    int pageNumber = serviceUtils.calcPageNumber(amount,qty);
    Page<Product> page = productRepository.findProductsByDepartment_Category(category, PageRequest.of(pageNumber,amount));

    return page.stream().collect(Collectors.toList());
  }

  public List<Product> importMasterFromCSV(List<Department> departments) {
    log.info("Importing from {}", csvImportFile);
    List<Product> products =
        fetchProductsFromCsv(
            csvImportFile,
            departments.stream().collect(Collectors.toMap(Department::getId, Function.identity())));
    log.info("Loaded {} Products", products.size());
    return products;
  }

  private List<Product> fetchProductsFromCsv(String path, Map<String, Department> departmentMap) {
    Map<String, Product> masterMap = new HashMap<>();
    try (Stream<String> lines = Files.lines(Paths.get(path))) {
      lines
          .skip(1)
          .map(this::csvLineToProduct)
          .filter(Objects::nonNull)
          .distinct()
          .forEach(d -> masterMap.put(d.getId(), d));
    } catch (IOException e) {
      log.warn("Error while processing the file: {}", e.getMessage());
    }
    //add dummy price up to 200.00 for products that have no price information
    return productRepository.saveAll(
        masterMap.values().stream().map(p->{
          if(p.getPrice()==null||p.getPrice()<=0){
            Random r = new Random();
            double val=r.nextDouble()*200;
            p.setPrice(BigDecimal.valueOf(val).setScale(2, RoundingMode.HALF_UP).doubleValue());
          }
          return p;
        }).collect(Collectors.toList()));
  }

  private Product csvLineToProduct(String line) {
    if (line.startsWith(";")) {
      line = line.substring(1);
    }
    String[] parts = line.split(";");
    if (parts.length != 10) {
      log.warn("Cannot parse line for Product with length {}", parts.length);
      return null;
    }
    // ID;DEPARTMENT_ID;DESCRIPTION_SHORT;DESCRIPTION_LONG;PRICE;RATING_STARS;RATING_STARS_TOTAL;RATING_REVIEWS;URL;IMAGE_URL
    return Product.builder()
        .id(parts[0])
        .department(parts[1].equals("null") ? null : Department.builder().id(parts[1]).build())
        .descriptionShort(parts[2])
        .descriptionLong(parts[3])
        .price(this.parseDouble(parts[4], null, "price"))
        .rating(parseProductRating(parts[5], parts[6], parts[7]))
        .url(parts[8])
        .imgUrl(parts[9])
        .build();
  }

  private Double parseDouble(String str, Double defaultValue, String forField) {
    try {
      return Double.parseDouble(str);
    } catch (Exception ignored) {
      log.warn("Cannot parse {} with value {}", forField, str);
    }
    return defaultValue;
  }

  private ProductRating parseProductRating(String stars, String totalStars, String totalRatings) {
    long totalRatingsLong = 0;
    try {
      totalRatingsLong = Long.parseLong(totalRatings);
    } catch (Exception e) {
      log.warn("Cannot parse long {} in product rating as total reviews", totalRatings);
    }
    return ProductRating.builder()
        .totalStars(parseDouble(totalStars, 0.0, "total stars in product rating"))
        .stars(parseDouble(stars, 0.0, "stars in product rating"))
        .totalRatings(totalRatingsLong)
        .build();
  }
}
