package com.kmarinos.amzmockproducts.service;

import java.util.Random;
import javax.persistence.Column;
import org.springframework.stereotype.Component;

@Component
public class ServiceUtils {

  public int calcPageNumber(int amount, long qty){
    int pageNumber = 0;
    int idx = 0;
    if(amount<qty){
      Random r = new Random();
      idx = r.nextInt((int)qty-amount);
    }
    pageNumber = (int)idx/amount;
    return pageNumber;
  }
}
