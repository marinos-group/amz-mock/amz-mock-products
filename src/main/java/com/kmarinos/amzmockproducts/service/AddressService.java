package com.kmarinos.amzmockproducts.service;

import com.kmarinos.amzmockproducts.api.dao.AddressDTO;
import com.kmarinos.amzmockproducts.api.dao.AddressPOST;
import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.repository.AddressRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AddressService {

  private final AddressRepository addressRepository;

  public List<Address> getAllAddresses(){
    return addressRepository.findAll();
  }
  public Address getAddressById(String id){
    if (id == null) {
      throw new EntityNotFoundException(Address.class, "id", null);
    }
    return addressRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(Address.class, "id", id));

  }
  public List<Address> getAddressesByCustomer(Customer customer){
    return addressRepository.findAddressByCustomer(customer);
  }

  public Address createAddressForCustomer(Customer customer, AddressPOST addressPOST) {

    List<Address> existingAddresses = this.getAddressesByCustomer(customer);
    //If this is the first ever address, set all flags to true
    if(existingAddresses.isEmpty()){
      addressPOST.setPrimaryAddress(true);
      addressPOST.setDeliveryAddress(true);
      addressPOST.setInvoiceAddress(true);
      //If any of the flags are true, the flags in the existing addresses need to be set to false
    }else{
      if(addressPOST.isDeliveryAddress()){
        existingAddresses.forEach(addr->{
          addr.setDeliveryAddress(false);
        });
      }
      if(addressPOST.isInvoiceAddress()){
        existingAddresses.forEach(addr->{
          addr.setInvoiceAddress(false);
        });
      }
      if(addressPOST.isPrimaryAddress()){
        existingAddresses.forEach(addr->{
          addr.setPrimaryAddress(false);
        });
      }
    }
    final var address = AddressDTO.POST(addressPOST);
    address.setCustomer(customer);
    return addressRepository.save(address);
  }
}
