package com.kmarinos.amzmockproducts.service.process;

import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;

@Slf4j
@RequiredArgsConstructor
@Data
public class ProcessingQueue {

  private final ProcessingPool processingPool;
  private final TaskScheduler taskScheduler;
  private final String name;

  private final Queue<Runnable> queue = new ConcurrentLinkedQueue<>();
  private ScheduledFuture<?> canRun;

  public void process(Runnable runnable) {

    queue.offer(runnable);
    if (canRun == null || canRun.isCancelled()) {
      canRun = taskScheduler.scheduleAtFixedRate(this::checkIfCanRun, new Date(), 1000);
    }
  }
    private void processQueue(){
    if(this.equals(processingPool.getCurrentlyProcessing())){
      Runnable toExecute = queue.poll();
      while(toExecute!=null){
        processingPool.getExecutorService().submit(toExecute);
        toExecute = queue.poll();
      }
      if(processingPool.canAcceptTasks()){
        System.err.println("Releasing pool ("+name+")");
        processingPool.setCurrentlyProcessing(null);
        canRun.cancel(false);
      }
    }
  }
  public void checkIfCanRun(){
    if(queue.size()!=0){
      System.out.println("Requesting access to pool with queued tasks: "+queue.size()+" ("+name+")");
    }
    if(processingPool.getCurrentlyProcessing()==null){
      System.err.println("Claiming pool ("+name+")");
      processingPool.setCurrentlyProcessing(this);
    }
    processQueue();
  }

}
