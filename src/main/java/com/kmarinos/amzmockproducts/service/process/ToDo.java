package com.kmarinos.amzmockproducts.service.process;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.annotations.GenericGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ToDo<T,R> {

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  String id;

  public ToDo(Class<T> class1,Class<R> class2){
    this.inputClazz=class1;
  }

  @Transient
  Class<T> inputClazz;
  String inputClass;
  byte[] inputBlob;
  String outputClass;
  byte[] outputBlob;
  String functionClass;
  byte[] functionBlob;

  public void execute() {
    switch(functionClass){
      case "java.util.function.Function":
        this.setOutput(this.getFunction().apply(this.getInput()));
        break;
      case "java.util.function.Consumer":
        this.getConsumer().accept(this.getInput());
        break;
      case "java.util.function.Supplier":
        this.setOutput(this.getSupplier().get());
        break;
    }
  }

  @SneakyThrows
  public T getInput() {
    if (this.inputClass == null) {
      return null;
    }
    Object input = byteArrayToObject(inputBlob);
    return (T) input;
  }

  @SneakyThrows
  public void setInput(T input) {
    if (input != null) {
      inputClass = input.getClass().getName();
      inputBlob = this.objectToByteArray((Serializable) input);
    }
  }

  @SneakyThrows
  public R getOutput() {
    if (this.outputClass == null) {
      return null;
    }
    Object output = byteArrayToObject(outputBlob);
    return (R) output;
  }

  @SneakyThrows
  public void setOutput(R output) {
    if(output!=null){
      outputClass = output.getClass().getName();
      outputBlob = this.objectToByteArray((Serializable)output);
    }
  }

  @SneakyThrows
  public Function<T, R> getFunction() {
    if (functionClass == null || functionBlob == null) {
      return (a) -> null;
    }
    return (Function<T, R>) byteArrayToObject(functionBlob);
  }
  @SneakyThrows
  public Consumer<T> getConsumer() {
    if (functionClass == null || functionBlob == null) {
      return (a) -> {};
    }
    return (Consumer<T>) byteArrayToObject(functionBlob);
  }
  @SneakyThrows
  public Supplier<R> getSupplier() {

    if (functionClass == null || functionBlob == null) {
      return () -> null;
    }
    return (Supplier<R>) byteArrayToObject(functionBlob);
  }

  @SneakyThrows
  public void setFunction(SerializableFunction<T, R> function) {
    functionClass = Function.class.getName();
    functionBlob = this.objectToByteArray(function);
  }
  @SneakyThrows
  public void setConsumer(SerializableConsumer<T> consumer){
    functionClass = Consumer.class.getName();
    functionBlob = this.objectToByteArray(consumer);
  }
  @SneakyThrows
  public void setSupplier(SerializableSupplier<R> supplier){
    System.err.println(inputClazz);
    functionClass = Supplier.class.getName();
    functionBlob = this.objectToByteArray(supplier);
  }

  private byte[] objectToByteArray(Serializable object) throws IOException {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(bos);
    oos.writeObject(object);
    oos.flush();
    return bos.toByteArray();
  }

  private Object byteArrayToObject(byte[] data) throws IOException, ClassNotFoundException {
    if (data == null) {
      return null;
    }
    ByteArrayInputStream in = new ByteArrayInputStream(data);
    ObjectInputStream is = new ObjectInputStream(in);
    return is.readObject();
  }

  public static interface SerializableFunction<T, R> extends Function<T, R>, Serializable {}
  public static interface SerializableConsumer<T> extends Consumer<T>, Serializable{}
  public static interface SerializableSupplier<R> extends Supplier<R>,Serializable{}
}
