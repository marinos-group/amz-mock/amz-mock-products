package com.kmarinos.amzmockproducts.service.process;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ProcessingPool {

  protected final ScheduledThreadPoolExecutor executorService;
  Object currentlyProcessing;

  protected boolean canAcceptTasks() {
    return executorService.getQueue().size() == 0;
  }
}
