package com.kmarinos.amzmockproducts.service.process;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ToDoRepository extends JpaRepository<ToDo<?,?>,String> {

}
