package com.kmarinos.amzmockproducts.service.process;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

@Component
public class ProcessingQueueFactory {

  private final ProcessingPool processingPool;
  private final TaskScheduler taskScheduler;

  public ProcessingQueueFactory(TaskScheduler taskScheduler){
    processingPool = new ProcessingPool(new ScheduledThreadPoolExecutor(6));

    this.taskScheduler = taskScheduler;
  }
  @Bean
  @Qualifier("customerQueue")
  public ProcessingQueue getCustomerQueue(){
    return new ProcessingQueue(processingPool, taskScheduler, "customerQueue");
  }
  @Bean
  @Qualifier("salesOrderQueue")
  public ProcessingQueue getSalesOrderQueue(){
    return new ProcessingQueue(processingPool, taskScheduler,
        "salesOrderQueue");
  }
  @Bean
  @Qualifier("debugQueue")
  public ProcessingQueue getDebugQueue(){
    return new ProcessingQueue(processingPool, taskScheduler, "debugQueue");
  }
}
