package com.kmarinos.amzmockproducts.service;

import com.kmarinos.amzmockproducts.api.dao.SalesOrderItemPOST;
import com.kmarinos.amzmockproducts.api.dao.SalesOrderPOST;
import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.OperationNotAllowedException;
import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.Product;
import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderHistory;
import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import com.kmarinos.amzmockproducts.model.SalesOrderItemHistory;
import com.kmarinos.amzmockproducts.model.SalesOrderItemStatus;
import com.kmarinos.amzmockproducts.model.SalesOrderStatus;
import com.kmarinos.amzmockproducts.repository.SalesOrderHistoryRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderItemHistoryRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderItemRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SalesOrderService {

  private final SalesOrderRepository salesOrderRepository;
  private final SalesOrderHistoryRepository salesOrderHistoryRepository;
  private final SalesOrderItemRepository salesOrderItemRepository;
  private final SalesOrderItemHistoryRepository salesOrderItemHistoryRepository;

  public List<SalesOrder> getAllSalesForCustomer(Customer customer) {
    return salesOrderRepository.findAllByCustomer(customer);
  }

  public SalesOrder getSalesOrderById(String id) {
    if (id == null) {
      throw new EntityNotFoundException(SalesOrder.class, "id", null);
    }

    final var salesOrder =
        salesOrderRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException(SalesOrder.class, "id", id));
    salesOrder.setSalesOrderItems(salesOrderItemRepository.getAllBySalesOrder(salesOrder));
    return salesOrder;
  }

  public List<SalesOrderItem> getSalesOrderItems(SalesOrder salesOrder) {
    return salesOrderItemRepository.getAllBySalesOrder(salesOrder);
  }

  public SalesOrder registerOrder(
      SalesOrderPOST salesOrderPOST, Customer customer, Function<String, Product> handleProduct) {

    var createOrderAt =
        salesOrderPOST.getSubmitOrderAt() == null
            ? LocalDateTime.now()
            : salesOrderPOST.getSubmitOrderAt();

    SalesOrder salesOrder = createSalesOrder(createOrderAt, customer);
    List<SalesOrderItem> salesOrderItems =
        createSalesOrderItems(salesOrderPOST, salesOrder, handleProduct);
    double price = salesOrderItems.stream().map(SalesOrderItem::getPrice).reduce(0.0, Double::sum);
    salesOrder.setInvoicedAmount(price);
    SalesOrderHistory soh = SalesOrderHistory.builder()
        .order(salesOrder)
        .status(SalesOrderStatus.OPEN)
        .createdAt(createOrderAt)
        .build();
    salesOrderHistoryRepository.save(soh);

    salesOrderItems.forEach(item->{
      SalesOrderItemHistory soih = SalesOrderItemHistory.builder()
          .orderItem(item)
          .status(SalesOrderItemStatus.OPEN)
          .createdAt(createOrderAt)
          .build();
      salesOrderItemHistoryRepository.save(soih);
    });
    salesOrder = this.getSalesOrderById(soh.getSalesOrderId());
    salesOrder.setSalesOrderItems(this.getSalesOrderItems(salesOrder));
    return salesOrder;
  }

  private List<SalesOrderItem> createSalesOrderItems(
      SalesOrderPOST salesOrderPOST,
      SalesOrder salesOrder,
      Function<String, Product> handleProduct) {
    List<SalesOrderItemPOST> salesOrderItemPOSTList = salesOrderPOST.getItems();
    List<SalesOrderItem> salesOrderItems = null;
    if (salesOrderItemPOSTList != null) {
      salesOrderItems =
          salesOrderItemPOSTList.stream()
              .map(soip -> createSalesOrderItem(soip, salesOrder, handleProduct))
              .filter(Objects::nonNull)
              .collect(Collectors.toList());
    }
    if (salesOrderItems == null || salesOrderItems.size() == 0) {
      throw new OperationNotAllowedException("Cannot register sales order with no items");
    }
    return salesOrderItems;
  }

  private SalesOrderItem createSalesOrderItem(
      SalesOrderItemPOST salesOrderItemPOST,
      SalesOrder salesOrder,
      Function<String, Product> handleProduct) {
    if (salesOrderItemPOST.getProduct() == null) {
      return null;
    }
    if (salesOrderItemPOST.getProduct().getId() == null) {
      return null;
    }
    Product product = handleProduct.apply(salesOrderItemPOST.getProduct().getId());
    if (product == null) {
      return null;
    }
    if (product.getPrice() <= 0) {
      return null;
    }
    long quantity = salesOrderItemPOST.getQuantity();
    if (quantity < 1) {
      return null;
    }

    return SalesOrderItem.builder()
        .product(product)
        .pricePerItem(product.getPrice())
        .quantity(quantity)
        .price(quantity * product.getPrice())
        .salesOrder(salesOrder)
        .createdAt(salesOrder.getCreatedAt())
        .build();
  }

  private SalesOrder createSalesOrder(LocalDateTime forDate, Customer customer) {
    return SalesOrder.builder()
        .customer(customer)
        .createdAt(forDate)
        .deliverTo(this.findDeliverTo(customer.getAddresses()))
        .invoiceTo(this.findInvoiceTo(customer.getAddresses()))
        .build();
  }

  private Address findDeliverTo(List<Address> addresses) {
    return getCorrectAddress(addresses.stream().filter(Address::isDeliveryAddress), addresses);
  }

  private Address findInvoiceTo(List<Address> addresses) {
    return getCorrectAddress(addresses.stream().filter(Address::isInvoiceAddress), addresses);
  }

  private Address getCorrectAddress(Stream<Address> addressStream, List<Address> addresses) {
    Address correctAddress = null;
    List<Address> candidateAddresses = addressStream.collect(Collectors.toList());
    if (candidateAddresses.size() >= 1) {
      Address firstCandidate = candidateAddresses.get(0);
      correctAddress =
          candidateAddresses.stream()
              .filter(Address::isPrimaryAddress)
              .findFirst()
              .orElse(firstCandidate);
    }
    return correctAddress;
  }
}
