package com.kmarinos.amzmockproducts.service;

import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.amzmockproducts.model.Department;
import com.kmarinos.amzmockproducts.repository.DepartmentRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DepartmentService {
  private final DepartmentRepository departmentRepository;

  @Value("${import.departments.file:exchange/departments_master.csv}")
  private String csvImportFile;

  public List<Department> importMasterFromCSV() {
    log.info("Importing from {}", csvImportFile);
    List<Department> departments = fetchDepartmentsFromCsv(csvImportFile);
    log.info("Loaded {} departments", departments.size());
    return departments;
  }
  public List<String> getAllCategories(){
    return departmentRepository.getAllCategories();
  }
  public List<Department> getAllDepartments(){
    return departmentRepository.findAll();
  }
  public Department getDepartmentById(String id){
    if (id == null) {
      throw new EntityNotFoundException(Department.class, "id", null);
    }
    Department department= departmentRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(Department.class, "id", id));
    department.setSubDepartments(departmentRepository.fetchSubDepartments(department));
    return department;
  }

  private List<Department> fetchDepartmentsFromCsv(String path) {
    Map<String, Department> masterMap = new HashMap<>();
    try (Stream<String> lines = Files.lines(Paths.get(path))) {
      lines
          .skip(1)
          .map(this::csvLineToDepartment)
          .filter(Objects::nonNull)
          .distinct()
          .forEach(d -> masterMap.put(d.getId(), d));
    } catch (IOException e) {
      log.warn("Error while processing the file: {}", e.getMessage());
    }
    Map<String, String> references =
        masterMap.values().stream()
            .collect(Collectors.toMap(Department::getId, d -> d.getParent().getId()));
    masterMap.values().forEach(child -> child.setParent(null));
    List<Department> departments = departmentRepository.saveAll(masterMap.values());
    departments.forEach(
        child -> {
          Department parent = masterMap.get(references.get(child.getId()));
          if (parent != null) {
            child.setParent(parent);
            parent.getSubDepartments().put(child.getId(), child);
          }
        });
    return departmentRepository.saveAll(departments);
  }

  private Department csvLineToDepartment(String line) {
    String[] parts = line.split(";");
    if (parts.length != 5) {
      log.warn("Cannot parse line for department with length {}", parts.length);
      return null;
    }
    return Department.builder()
        .id(parts[0])
        .category(parts[1])
        .name(parts[2])
        .parent(Department.builder().id(parts[3]).build())
        .bestsellerUrl(parts[4])
        .build();
  }
}
