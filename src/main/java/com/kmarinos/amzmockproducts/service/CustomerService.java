package com.kmarinos.amzmockproducts.service;

import com.kmarinos.amzmockproducts.api.dao.CustomerDTO;
import com.kmarinos.amzmockproducts.api.dao.CustomerPOST;
import com.kmarinos.amzmockproducts.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.repository.CustomerRepository;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;
  private final ServiceUtils serviceUtils;

  public List<Customer> getAllCustomers() {
    return customerRepository.findAll();
  }

  public Customer getCustomerById(String id) {
    return this.getCustomerById(id, customer -> null);
  }

  public Customer createCustomerPlaceholder() {
    return customerRepository.save(Customer.builder().build());
  }

  @Transactional
  public Customer createCustomer(CustomerPOST customerPOST, Consumer<Customer> customerCallback) {

    Customer customer = CustomerDTO.POST(customerPOST);
    while (customerRepository.existsCustomerByEmail(customer.getEmail())) {
      String suffix = "" + new Random().nextInt(10);
      String[] parts = customer.getEmail().split("@");
      String newEmail = customer.getEmail().replace(parts[0], parts[0] + suffix);
      customer.setEmail(newEmail);
    }
    customerRepository.save(customer);
    customerCallback.accept(customer);
    return customer;
  }

  public Customer getCustomerById(String id, Function<Customer, List<Address>> addressesSupplier) {
    if (id == null) {
      throw new EntityNotFoundException(Customer.class, "id", null);
    }

    final var customer =
        customerRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException(Customer.class, "id", id));
    customer.setAddresses(addressesSupplier.apply(customer));
    return customer;
  }

  public List<Customer> getRandomCustomers(int amount) {
    long qty = customerRepository.count();
    int pageNumber =serviceUtils.calcPageNumber(amount,qty);
    Page<Customer> page = customerRepository.findAll(PageRequest.of(pageNumber, amount));
    return page.stream().collect(Collectors.toList());
  }

}
