package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.SalesOrderItemHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesOrderItemHistoryRepository extends JpaRepository<SalesOrderItemHistory,String> {

}
