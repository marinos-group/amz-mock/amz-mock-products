package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.Department;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DepartmentRepository extends JpaRepository<Department,String> {

  default Map<String,Department> fetchSubDepartments(Department parent){
    return findDepartmentsByParent(parent).stream().collect(Collectors.toMap(Department::getId,
        Function.identity()));
  }
  List<Department> findDepartmentsByParent(Department parent);

  @Query("select distinct d.category from Department d")
  List<String> getAllCategories();

}
