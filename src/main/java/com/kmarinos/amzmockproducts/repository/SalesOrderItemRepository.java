package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesOrderItemRepository extends JpaRepository<SalesOrderItem,String> {

  List<SalesOrderItem> getAllBySalesOrder(SalesOrder salesOrder);
}
