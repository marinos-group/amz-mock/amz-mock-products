package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderHistory;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesOrderHistoryRepository extends JpaRepository<SalesOrderHistory,String> {

}
