package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,String> {

  boolean existsCustomerByEmail(String email);
}
