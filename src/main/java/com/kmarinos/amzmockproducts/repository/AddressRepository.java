package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,String> {

  public List<Address> findAddressByCustomer(Customer customer);
}
