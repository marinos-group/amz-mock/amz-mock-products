package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.Product;
import java.util.List;
import java.util.stream.Stream;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, String> {

  @Query("select p from Product p where p.department.category=:category")
  List<Product> findProductsByCategory(@Param("category") String category);

  @Query("select p from Product p")
  Stream<Product> findAllProducts();

  long countProductsByDepartment_Category(String category);

  Page<Product> findProductsByDepartment_Category(String category, Pageable pageable);
}
