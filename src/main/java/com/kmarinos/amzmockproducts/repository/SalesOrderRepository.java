package com.kmarinos.amzmockproducts.repository;

import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.SalesOrder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesOrderRepository extends JpaRepository<SalesOrder,String> {

  List<SalesOrder> findAllByCustomer(Customer customer);

}
