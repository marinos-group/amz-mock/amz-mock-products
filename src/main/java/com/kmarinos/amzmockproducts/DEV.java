package com.kmarinos.amzmockproducts;

import com.github.javafaker.Faker;
import com.kmarinos.amzmockproducts.model.Address;
import com.kmarinos.amzmockproducts.model.Customer;
import com.kmarinos.amzmockproducts.model.Department;
import com.kmarinos.amzmockproducts.model.Product;
import com.kmarinos.amzmockproducts.model.ProductRating;
import com.kmarinos.amzmockproducts.model.SalesOrder;
import com.kmarinos.amzmockproducts.model.SalesOrderHistory;
import com.kmarinos.amzmockproducts.model.SalesOrderItem;
import com.kmarinos.amzmockproducts.model.SalesOrderItemHistory;
import com.kmarinos.amzmockproducts.model.SalesOrderItemStatus;
import com.kmarinos.amzmockproducts.model.SalesOrderStatus;
import com.kmarinos.amzmockproducts.repository.AddressRepository;
import com.kmarinos.amzmockproducts.repository.CustomerRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderHistoryRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderItemHistoryRepository;
import com.kmarinos.amzmockproducts.repository.SalesOrderRepository;
import com.kmarinos.amzmockproducts.service.DepartmentService;
import com.kmarinos.amzmockproducts.service.ProductService;
import com.kmarinos.amzmockproducts.service.process.ToDo;
import com.kmarinos.amzmockproducts.service.process.ToDoRepository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class DEV {

  private final AmzClient client;
  private final DepartmentService departmentService;
  private final ProductService productService;
  private final CustomerRepository customerRepository;
  private final AddressRepository addressRepository;
  private final SalesOrderRepository salesOrderRepository;
  private final SalesOrderHistoryRepository salesOrderHistoryRepository;
  private final SalesOrderItemHistoryRepository salesOrderItemHistoryRepository;
  private final ToDoRepository toDoRepository;


  @SneakyThrows
  @EventListener(ApplicationReadyEvent.class)
  public void init() {
    // fetch all departments and save them to csv

    /*   client.fetchAllDepartments(departmentToCsv("exchange/departments.csv"));*/

    // fetch all products
    /*for (Department department : ProgressBar.wrap(departments,"Fetching products")) {
      if (department.getBestsellerUrl() == null || department.getBestsellerUrl().isEmpty()) {
        continue;
      }
      productsToCsv(
          client.fetchAllProducts(
              department, 10),
          "exchange/products.csv");
    }*/
    List<Department> departments = departmentService.getAllDepartments();
    if (departments.size() == 0) {
      departments = departmentService.importMasterFromCSV();
    }
    List<Product> products = productService.getAllProducts();
    if (products.size() == 0) {
      products = productService.importMasterFromCSV(departments);
    }
    /* **************** TEST CUSTOMER ********************/
//    Customer customer = createDummyCustomer();
    /* **************** TEST SALES ********************/
//    createDummySales(products, customer);

    //    printFakerExamples(()->faker.address().city());

    /* **************** TEST ToDo ********************/
    ToDo<String,String> todo = new ToDo<>();
    todo.setInput("potato");
    todo.setSupplier(() -> "Hallo");
    toDoRepository.save(todo);
    Thread.sleep(1000);

    ToDo<?,?> todo1=toDoRepository.findById(todo.getId()).get();
    todo1.execute();
    System.out.println(todo1.getOutput());
    toDoRepository.save(todo1);
  }

  private void createDummySales(List<Product> products, Customer customer) {
    SalesOrder order = SalesOrder.builder().customer(customer).build();
    //    salesOrderRepository.save(order);
    SalesOrderHistory h =
        SalesOrderHistory.builder().order(order).status(SalesOrderStatus.OPEN).build();
    salesOrderHistoryRepository.save(h);

    SalesOrderItem soi1 =
        SalesOrderItem.builder()
            .salesOrder(order)
            .product(products.get(0))
            .quantity(1)
            .pricePerItem(products.get(0).getPrice())
            .price(products.get(0).getPrice())
            .build();

    SalesOrderItemHistory soih1 =
        SalesOrderItemHistory.builder().orderItem(soi1).status(SalesOrderItemStatus.OPEN).build();
    salesOrderItemHistoryRepository.save(soih1);

    SalesOrderItem soi2 =
        SalesOrderItem.builder()
            .salesOrder(order)
            .product(products.get(1))
            .quantity(2)
            .pricePerItem(products.get(1).getPrice())
            .price(2 * products.get(1).getPrice())
            .build();

    SalesOrderItemHistory soih2 =
        SalesOrderItemHistory.builder().orderItem(soi2).status(SalesOrderItemStatus.OPEN).build();
    salesOrderItemHistoryRepository.save(soih2);

    SalesOrderItemHistory soih2_1 =
        SalesOrderItemHistory.builder()
            .orderItem(soi2)
            .status(SalesOrderItemStatus.APPROVED)
            .build();
    salesOrderItemHistoryRepository.save(soih2_1);

    SalesOrderHistory h2 =
        SalesOrderHistory.builder().order(order).status(SalesOrderStatus.APPROVED).build();
    salesOrderHistoryRepository.save(h2);
  }

  public Customer createDummyCustomer() {
    Faker faker = new Faker(new Locale("de-DE"));
    Customer customer =
        Customer.builder()
            .firstname(faker.name().firstName())
            .lastname(faker.name().lastName())
            .username(faker.name().username())
            .email(faker.internet().safeEmailAddress())
            .active(true)
            .build();
    customerRepository.save(customer);

    Address address1 =
        Address.builder()
            .city(faker.address().city())
            .countryCode(faker.address().countryCode())
            .customer(customer)
            .deliveryAddress(true)
            .primaryAddress(true)
            .invoiceAddress(true)
            .street(faker.address().streetName())
            .streetNumber(faker.address().streetAddressNumber())
            .zipCode(faker.address().zipCode())
            .state(faker.address().state())
            .build();

    Address address2 =
        Address.builder()
            .city(faker.address().cityName())
            .countryCode(faker.address().countryCode())
            .customer(customer)
            .deliveryAddress(false)
            .primaryAddress(false)
            .invoiceAddress(false)
            .street(faker.address().streetName())
            .streetNumber(faker.address().streetAddressNumber())
            .zipCode(faker.address().zipCode())
            .state(faker.address().state())
            .build();
    addressRepository.save(address1);
    addressRepository.save(address2);

    return customer;
  }

  public void printFakerExamples(Supplier<String> supplier) {
    List<String> str =
        IntStream.range(1, 1000).mapToObj(i -> supplier.get()).collect(Collectors.toList());
    Map<String, Long> map = new HashMap<>();
    str.forEach(
        val -> {
          map.put(val, map.getOrDefault(val, 0L) + 1);
        });
    map.forEach((key, value) -> log.info("{} ({})", key, value));
    log.info("Done.");
  }

  public void productsToCsv(List<Product> products, String path) {
    File csv = new File(path);
    csv.getParentFile().mkdirs();
    try {
      if (csv.createNewFile()) {
        Writer output;
        output = new BufferedWriter(new FileWriter(csv, true));
        output.append(
            "ID;DEPARTMENT_ID;DESCRIPTION_SHORT;DESCRIPTION_LONG;PRICE;RATING_STARS;RATING_STARS_TOTAL;RATING_REVIEWS;URL;IMAGE_URL");
        output.close();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
    Writer output;
    try {
      output = new BufferedWriter(new FileWriter(csv, true));
      products.forEach(
          p -> {
            try {
              output.append(this.productToCSVLine(p));
            } catch (IOException e) {
              e.printStackTrace();
            }
          });
      output.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Consumer<Department> departmentToCsv(String path) {
    File csv = new File(path);
    csv.getParentFile().mkdirs();
    try {
      if (csv.createNewFile()) {
        Writer output;
        output = new BufferedWriter(new FileWriter(csv, true));
        output.append("ID;CATEGORY;NAME;PARENT_ID;URL");
        output.close();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
    log.info("Save departments to csv:{}", csv.getAbsolutePath());
    return department -> {
      Writer output;
      try {
        output = new BufferedWriter(new FileWriter(csv, true));
        department
            .streamSubDepartments()
            .filter(d -> d.getId() != null && !d.getId().isEmpty())
            .map(this::departmentToCSVLine)
            .forEach(
                s -> {
                  try {
                    output.append(s);
                  } catch (IOException e) {
                    e.printStackTrace();
                  }
                });
        output.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    };
  }

  private String productToCSVLine(Product product) {
    return System.lineSeparator()
        + product.getId()
        + ";"
        + (product.getDepartment() != null ? product.getDepartment().getId() : "")
        + ";"
        + product.getDescriptionShort()
        + ";"
        + product.getDescriptionLong()
        + ";"
        + product.getPrice()
        + ";"
        + productRatingToCSV(product.getRating())
        + ";"
        + product.getUrl()
        + ";"
        + product.getImgUrl();
  }

  private String productRatingToCSV(ProductRating rating) {
    if (rating == null) {
      return ";;";
    }
    return rating.getStars() + ";" + rating.getTotalStars() + ";" + rating.getTotalRatings();
  }

  private String departmentToCSVLine(Department department) {
    return System.lineSeparator()
        + department.getId()
        + ";"
        + department.getCategory()
        + ";"
        + department.getName()
        + ";"
        + (department.getParent() == null ? "" : department.getParent().getId())
        + ";"
        + department.getBestsellerUrl();
  }
}
